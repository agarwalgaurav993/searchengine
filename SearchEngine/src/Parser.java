import java.io.File;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
public class Parser {
	static String XML_FILE_PATH = "/home/gaurav/eclipse-workspace/TempSearch/data/wiki-search-small.xml";
	public static void setXML_FILE_PATH(String xML_FILE_PATH) {
		XML_FILE_PATH = xML_FILE_PATH;
	}
	
	public static void parse() {
		File inputFile = new File(XML_FILE_PATH);
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			XMLUserHandler handler = new XMLUserHandler();
			saxParser.parse(inputFile, handler);
		}
		catch(OutOfMemoryError e) {
			System.out.println("Sorry.. memory error java heap size exceeded");
		}
		catch(Exception e) {
			e.printStackTrace();
			//System.out.println("Unknown error at parsing");
		}
	}
}
