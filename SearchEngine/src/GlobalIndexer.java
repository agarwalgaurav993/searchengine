import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

public class GlobalIndexer {
	static TreeMap<String, TreeMap<String, TreeMap<String, Long> > > gMap = new TreeMap<String, TreeMap<String, TreeMap<String, Long> > >();
	public static String DOCUMENT_FOLDER = "Docs";
	public static String FINAL_FOLDER = "MultiLevel";
	public static String TITLE_FOLDER = "Title";
	public static TreeMap<String, String> titlemap = new TreeMap<String, String>();
	public static long counter = 0;
	static long dict_size = 0;
	static long posting_size = 0;
	static long size_limit = 1024*1024*25;
	public static long doc_counter = 0;
	public static TreeMap<String, TreeMap<String, TreeMap<String, Long> > > getgMap() {
		return gMap;
	}
	public static String getCounter() {
		return Long.toString(counter);
	}
	public static void setCounter() {
		GlobalIndexer.counter++;
	}
	public static void clearMap() {
		gMap.clear();
		posting_size = 0;
		dict_size = 0;
	}
	
	public static boolean addPage(Page P) {
		doc_counter++;
		TreeMap<String, TreeMap<String, Long> > t = P.getFieldMap();
		titlemap.put(P.getId(), P.title);
		posting_size += t.size()*15;
		String doc = P.getId();
		for(Map.Entry<String, TreeMap<String, Long> > e : t.entrySet()) {
			String word = e.getKey();
			if(!gMap.containsKey(word)) {
				gMap.put(word, new TreeMap<String, TreeMap<String, Long > >());
			}
			TreeMap<String, TreeMap<String, Long> > temp = gMap.get(word);
			temp.put(doc, e.getValue());
		}
		dict_size = gMap.size();
		if(dict_size + posting_size > size_limit) {
			writeToFile(DOCUMENT_FOLDER);
			writeTitleFile(TITLE_FOLDER);
			clearMap();
		}
		
		return true;
	}
	
	public static void writeTitleFile(String Folder) {
		//updateX();
		try{
			File directory = new File(Folder);
		    if (! directory.exists()){
		        directory.mkdir();
		    }
		    String filename = Folder + File.separatorChar + "0_"+ getCounter() +".txt";
		    PrintWriter writer = new PrintWriter(new FileOutputStream(new File(filename),true));
			String line = "";
			for(Map.Entry<String, String> e : titlemap.entrySet()) {
				line = e.getKey() + "=" + e.getValue();
				writer.println(line);
			}
		    writer.close();
		}
		catch(IOException e) {
			System.out.println("Error: In writeToFile :Index Creation");
		}
		titlemap.clear();
	}
	
	
	public static void writeToFile(String Folder) {
		try{
			File directory = new File(Folder);
		    if (! directory.exists()){
		        directory.mkdir();
		    }
		    setCounter();
		    String filename = Folder + File.separatorChar + "file_" + getCounter() + ".txt";
		    PrintWriter writer = new PrintWriter(filename, "UTF-8");
			String line = "";
			for(Map.Entry<String, TreeMap<String, TreeMap<String, Long> > > e : gMap.entrySet()) {
				line += e.getKey() + "=";
				TreeMap<String, TreeMap<String, Long> > v = e.getValue();
				for(Map.Entry<String, TreeMap<String, Long> > et : v.entrySet()) {
					line += et.getKey() + ":";
					TreeMap<String, Long> vt = et.getValue();
					for(Map.Entry<String, Long> ett : vt.entrySet()) {
						//line += ett.getKey()+"-"+ ett.getValue().toString()+",";
						line += ett.getValue().toString()+",";
					}
					line = line.substring(0, line.length() - 1) + "|";
				}
				line = line.substring(0, line.length() - 1);
				writer.println(line);
				line = "";
				//System.gc();
			}
			//Close File
			writer.close();
		}
		catch(IOException e) {
			System.out.println("Error: In writeToFile :Index Creation");
		}
	}
}
