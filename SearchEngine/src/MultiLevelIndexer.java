import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MultiLevelIndexer {
	int currLevel;
	int nextLevel;
	int nf;
	TreeMap<String, String> index;
	String dir;
	MultiLevelIndexer(String directory){
		currLevel = 0;
		nextLevel = 1;
		nf = 0;
		index = new TreeMap<String, String>();
		dir = directory;
	}
	void reset() {
		index.clear();
		nf = 0;
	}
	public static void writeToFile(String Folder, int level, int fn, TreeMap<String, String> mp) {
		try{
			File directory = new File(Folder);
		    if (! directory.exists()){
		        directory.mkdir();
		    }
		    String filename = Folder + File.separatorChar + String.valueOf(level) + "_" + String.valueOf(fn) + ".txt";
		    PrintWriter writer = new PrintWriter(filename, "UTF-8");
			String line = "";
			for(Map.Entry<String,String > e : mp.entrySet()) {
				line += e.getKey() + "=" + e.getValue();
				writer.println(line);
				line = "";
			}
			//Close File
			writer.close();
		}
		catch(IOException e) {
			System.out.println("Error: In writeToFile :Index Creation");
		}
	}

	void CreateLevel(List<File> l) {
		int limit = 1024;
		for(File f : l) {		
			try {
				BufferedReader br = new BufferedReader(new FileReader(f));
				String line = br.readLine();
				br.close();
				if(line.isEmpty())
					throw new IOException();
				String Key = line.split("=")[0];
				index.put(Key, f.getName());
				if(index.size() >= limit) {
					++nf;
					writeToFile(dir, nextLevel, nf, index);
					index.clear();
				}
			}
			catch(IOException e) {
			
			}
		}
		if(!index.isEmpty()) {
			++nf;
			writeToFile(dir, nextLevel, nf, index);
			index.clear();
		}
	}
	
	void Indexer() {
		List<File> l = FileHandler.getFiles(dir, String.valueOf(currLevel));
		while(l.size() > 1) {
			CreateLevel(l);
			currLevel = nextLevel;
			nextLevel++;
			l = FileHandler.getFiles(dir, String.valueOf(currLevel));
		}
	}
}
