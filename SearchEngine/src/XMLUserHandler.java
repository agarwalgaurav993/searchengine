import org.xml.sax.helpers.DefaultHandler;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
public class XMLUserHandler extends DefaultHandler{
	boolean text, validID, id, title;
	Page p;
	StringBuilder currentText;
	PorterStemmer stemmer;
	int cnt;
	XMLUserHandler() {
		text = validID = id = false;
		currentText = new StringBuilder();
		stemmer = new PorterStemmer();
		cnt = 0;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("page")) {
			validID = true;
			p = new Page(stemmer);
		}
		else if(validID && qName.equalsIgnoreCase("id")) {
			id = true;
		}
		else if(qName.equalsIgnoreCase("title")) {
			title = true;
		}
		else if(qName.equalsIgnoreCase("redirect")) {
			p.title = attributes.getValue("title");
		}
		else if(qName.equalsIgnoreCase("text")) {
			text = true;
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(text) {
			p.text = currentText.toString();
		}
		else if(id) {
			p.id = currentText.toString();
			validID = false;
		}
		else if(title) {
			p.title = currentText.toString();
		}
		if(qName.equalsIgnoreCase("page")) {
			
			//Work on Page p
			p.extractInfo();
			GlobalIndexer.addPage(p);
			cnt++;
			if(cnt == 50000) {
				System.out.println( GlobalIndexer.doc_counter + " Files evalutaed\n");
				cnt = 0;
			}
		}
		text = id = title = false;
		currentText.setLength(0);
	}
	
	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		if(id || text || title) {
			currentText.append(ch, start, length);
		}
	}
}
