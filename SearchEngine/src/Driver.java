
public class Driver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("jdk.xml.totalEntitySizeLimit", String.valueOf(Integer.MAX_VALUE));
		Parser.setXML_FILE_PATH(args[0]);
		Parser.parse();
		if(!GlobalIndexer.getgMap().isEmpty()) {
			GlobalIndexer.writeToFile(GlobalIndexer.DOCUMENT_FOLDER);
			GlobalIndexer.clearMap();
			GlobalIndexer.writeTitleFile(GlobalIndexer.TITLE_FOLDER);
		}
		System.out.println(GlobalIndexer.doc_counter);
		
	}

}
