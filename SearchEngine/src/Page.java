import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Page {
	public String title, id, text;
	TreeMap<String, TreeMap<String, Long> > fieldMap;
	PorterStemmer stemmer;
	Page(PorterStemmer s){
		stemmer = s;
		fieldMap = new TreeMap<String, TreeMap<String, Long> >();
	}
	public String getId() {
		return id;
	}

	public TreeMap<String, TreeMap<String, Long>> getFieldMap() {
		return fieldMap;
	}
	String []ar = {"x", "t", "b", "i", "c"};
	
	String ValidWord(String word, boolean chk) {
		if(word == "\n" || word == " " || word == "\t" || word == "\r" || word == null
				|| word.isEmpty())
			return "";
		if(chk && StopWords.isStopWord(word))
			return "";
		
		String stemmedWord = word;
		try {
			stemmedWord = stemmer.stem(word);
		}
		catch(Exception e) {
			;
		}
		if(stemmedWord.equalsIgnoreCase("No term entered") || stemmedWord.equalsIgnoreCase("Invalid term"))
			return "";
		return stemmedWord;
	}
	
	TreeMap<String, Long> getBasicStruct(){
		TreeMap<String, Long> t = new TreeMap<String, Long>();
		for(String s: ar) {
			t.put(s, (long)0);
		}
		return t;
	}
	void fieldMapUpdater(String text, String f, boolean chk) {
		String []words = text.split("[^a-zA-Z0-9]");
		//text = text.replaceAll("[!?,]'", "");
		//String[] words = text.split("\\s+");
		for(String oneWord : words) {
			oneWord = ValidWord(oneWord, chk);
			if(oneWord != null && !oneWord.isEmpty() && oneWord.length() != 2) {
				if(!fieldMap.containsKey(oneWord)) { 
					fieldMap.put(oneWord, getBasicStruct());
				}
				TreeMap<String, Long> h = fieldMap.get(oneWord);
				long val = (h.get(f));
				h.put(f, val + 1);
			}
		}
	}
	void infoboxAdder(String text) {
		String t = "";
		String regex1 = "\\{\\{infobox";
		String regex2 = "\\}\\}";
		Matcher m1 = Pattern.compile(regex1).matcher(text);
		Matcher m2 = Pattern.compile(regex2).matcher(text);
		while (m1.find())
			if(m2.find(m1.start())){
				t += text.substring(m1.end(), m2.start());
			}
		fieldMapUpdater(t, "i", true);
	}
	void catagoryAdder(String text) {
		String t = "";
		String regex1 = "\\[\\[category:";
		String regex2 = "\\]\\]";
		Matcher m1 = Pattern.compile(regex1).matcher(text);
		Matcher m2 = Pattern.compile(regex2).matcher(text);
		while (m1.find())
			if(m2.find(m1.start())){
				t += text.substring(m1.end(), m2.start()) + " ";
			}
		fieldMapUpdater(t, "c", true);
	}
	void titleAdder(String text) {
		fieldMapUpdater(text, "t", false);
	}
	void bodyAdder(String text) {
		fieldMapUpdater(text, "b", true);
	}
	void linksAdder(String text) {
		
	}
	void referencesAdder(String text) {
		
	}
	
	void frequencyMapUpdater() {
		Collection<TreeMap<String, Long> > c = fieldMap.values();
	    Iterator<TreeMap<String, Long> > itr = c.iterator();
	    while(itr.hasNext()) {
	    	TreeMap<String, Long> h = itr.next();
	    	h.put("x", h.get("t") + h.get("b"));
	    }
	}
	
	void tfUpdater() {
		double normalizer = 0;
		Collection<TreeMap<String, Long> > c = fieldMap.values();
	    Iterator<TreeMap<String, Long> > itr = c.iterator();
	    while(itr.hasNext()) {
	    	long h = itr.next().get("x");
	    	normalizer += (h*h);
	    }
	    normalizer = Math.round(Math.sqrt(normalizer)*100.0)/100.0;
	    
	    itr = c.iterator();
	    while(itr.hasNext()) {
	    	TreeMap<String, Long> h = itr.next();
	    	long tf = Math.round((((double)h.get("x")/normalizer)*1000));
	    	h.put("x", tf);
	    }
	}
	public int extractInfo(){
		//System.out.println(title);
		if(!title.isEmpty())
			title = title.toLowerCase();
		id = id.toLowerCase();
		if(!text.isEmpty())
			text = text.toLowerCase();
		bodyAdder(text);
		titleAdder(title);
		catagoryAdder(text);
		infoboxAdder(text);
		linksAdder(text);
		referencesAdder(text);
		frequencyMapUpdater();
		tfUpdater();
		text = "";
		//System.gc();
		return 0;
	}
}
