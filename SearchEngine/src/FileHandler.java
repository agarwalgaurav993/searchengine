import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;


public class FileHandler {

	private static class Filter implements FilenameFilter{
		String prefix;
		Filter(String prefix){
			this.prefix = prefix;
		}
		
		@Override
		public boolean accept(File directory, String filename) {
			return filename.startsWith(prefix);
		}
	}
	public static List<File> getFiles(String dirPath, String prefix){
		File path = new File(dirPath);
		List<File> result = new ArrayList<File>();
		FilenameFilter filter = new Filter(prefix);
		String []children = path.list(filter);
		if(children != null) {
			for(int i=0; i<children.length; i++) {
				String filename = children[i];
				filename = path.getAbsolutePath() + File.separator + filename;
				result.add(new File(filename));
			}
		}
		return result;
	}
}
