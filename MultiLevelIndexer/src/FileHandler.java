import java.io.File;
import java.io.FilenameFilter;


public class FileHandler {

	private static class Filter implements FilenameFilter{
		String prefix;
		Filter(String prefix){
			this.prefix = prefix;
		}
		
		@Override
		public boolean accept(File directory, String filename) {
			return filename.startsWith(prefix);
		}
	}
	public static String[] getFiles(String dirPath, String prefix){
		File path = new File(dirPath);
		FilenameFilter filter = new Filter(prefix);
		String []children = path.list(filter);
		if(children != null) {
			for(int i=0; i<children.length; i++) {
				String filename = children[i];
				filename = path.getAbsolutePath() + File.separator + filename;
				children[i] = filename;
				
			}
		}
		return children;
	}
}
