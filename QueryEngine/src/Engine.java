import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class Engine{
	public TreeMap<String, Long > mp;
	//public TreeMap<String, String> dt;
	public int levels;
	public int currlevel;
	public int tlevel;
	public int ctlevel;
	public static long n = 17640866;
	String dir;
	Engine(String directory, String indexFile, String tindexFile){
		mp = new TreeMap<String, Long>();
		dir = directory;
		//setLevel();
		//System.out.println(indexFile);
		levels = Integer.parseInt(indexFile.split("_")[0]);
		currlevel = levels;
		tlevel = Integer.parseInt(tindexFile.split("_")[0]);
		ctlevel = tlevel;
	}
	
	
	void constructDocMap(TreeMap<String, TreeMap<String, Long> > t, String line) {
		String []ar = { "b", "c",  "i", "t", "x"};
		String value = line.split("=")[1];
		//TreeMap<String, TreeMap<String, Long> > t = new TreeMap<String, TreeMap<String, Long> >();
		String[] doc = value.split("\\|");
		long gaurav = doc.length;
		for(String d : doc) {
			if(d.length() == 0 || d == "" || d == null)
				continue;
			String docID = d.split(":")[0];
			String fields = d.split(":")[1];
			TreeMap<String, Long> f = new TreeMap<String, Long>();
			String[] vals = fields.split(",");
			for(int i=0; i<vals.length; i++) {
				f.put(ar[i], new Long(vals[i]));
			}
			gaurav = (long)Math.log10((double)n/gaurav)*f.get("x");
			t.put(docID, f);
		}
	}
	
	TreeMap<String, TreeMap<String, Long> > getPosting(String word){
		//Reach Level 0
		String f_name = String.valueOf(currlevel) + "_" + "1.txt";
		String prev = "";
		while(currlevel != -1) {
			f_name = dir + File.separatorChar + f_name;
			File f = new File(f_name);
			try {
				BufferedReader br = new BufferedReader(new FileReader(f));
				String line;
				while((line=br.readLine()) != null)
				{
					String Key = line.split("=")[0];
					if(Key.compareToIgnoreCase(word) > 0) {
						/*Close Previous, new Level*/
						currlevel--;
						if(prev.indexOf('=') != -1)
							f_name = prev.split("=")[1];
						break;
					}
					prev = line;
				}
				br.close();
				
			}
			catch(IOException e) {
				break;
			}
		}
		
		TreeMap<String, TreeMap<String, Long> > t = new TreeMap<String, TreeMap<String, Long> >();
		if(currlevel == -1 && word.equalsIgnoreCase(prev.split("=")[0])) {
			//Tokenize and return posting list
			constructDocMap(t, prev);
		}
		//Linearly find Posting List for it
		return t;
	}
	
	String getDocName(String word){
		//Reach Level 0
		String dir = "Title";
		String f_name = String.valueOf(ctlevel) + "_" + "1.txt";
		String prev = "";
		while(ctlevel != -1) {
			f_name = dir + File.separatorChar + f_name;
			File f = new File(f_name);
			try {
				BufferedReader br = new BufferedReader(new FileReader(f));
				String line;
				while((line=br.readLine()) !=  null)
				{
					String Key = line.split("=")[0];
					if(Key.compareToIgnoreCase(word) > 0) {
						/*Close Previous, new Level*/
						ctlevel--;
						if(prev.indexOf('=') != -1)
							f_name = prev.split("=")[1];
						break;
					}
					prev = line;
				}
				br.close();
				
			}
			catch(IOException e) {
				break;
			}
		}
		if(ctlevel == -1) {
			//Tokenize and return posting list
			ctlevel = tlevel;
			return f_name;
		}
		//Linearly find Posting List for it
		ctlevel = tlevel;
		return "Title Missing";
	}
	
	
	TreeMap<String, Long> getDocuments(TreeMap<String, TreeMap<String, Long> > posting, String field){
		TreeMap<String, Long> l = new TreeMap<String, Long>();
		for(Map.Entry<String, TreeMap<String,Long > > e : posting.entrySet()) {
			if(field == "n") {
				l.put(e.getKey(), e.getValue().get("x"));
			}
			else if((long)e.getValue().get(field) != 0){
				l.put(e.getKey(), e.getValue().get("x"));
			}
		}
		return l;
	}
	public void addThisMap(TreeMap<String, Long> t) {
		for(Map.Entry<String,Long > e : t.entrySet()) {
			Long val = e.getValue();
			if(mp.containsKey(e.getKey())) {
				val += mp.get(e.getKey());
			}
			mp.put(e.getKey(), val);
		}
	}
	public TreeMap<String, String> processQuery(String s) {
		s = s.toLowerCase();
		s = s.replaceAll("[!?,]'", " ");
		String []ar = s.split(" ");
		TreeMap<String, String> res = new TreeMap<String, String>();
		String type = "n";
		for(int i=0; i<ar.length; i++) {
			if(ar[i].indexOf(":") != -1) {
				String []temp = ar[i].split(":");
				type = temp[0];
				ar[i] = temp[1];
			}
			res.put(ar[i], type);
		}
		return res;
	}
	
	public static void main(String argv[]) {
		String indexFile = argv[0];
		String pathToIndexDirectory = argv[1];
		String docIDMapper = argv[2];
		Engine e = new Engine(pathToIndexDirectory, indexFile, docIDMapper);
		
		System.out.println("Enter Your Query ...");
		Scanner sc=new Scanner(System.in);
		String q = sc.nextLine();
		sc.close();
		double start= System.currentTimeMillis();
		TreeMap<String, String> query = e.processQuery(q);
		for(Entry<String, String> et : query.entrySet()) {
			String key = et.getKey();
			String field = et.getValue();
			TreeMap<String, TreeMap<String, Long> > postings = e.getPosting(key);
			TreeMap<String, Long> docs = e.getDocuments(postings, field);
			e.addThisMap(docs);
			e.currlevel = e.levels;
		}
		TreeMap<Long, Set<String> > res = new TreeMap<Long, Set<String> >();
		for(Entry<String, Long> et : e.mp.entrySet()) {
			if(res.containsKey(-(long)et.getValue())) {
				res.get(-(long)et.getValue()).add(et.getKey());
			}
			else {
				
				res.put(-(long)et.getValue(), new HashSet<String>());
				res.get(-(long)et.getValue()).add(et.getKey());
			}
			
		}
		System.out.println("Results ... ");
		int c = 0;
		for(Entry<Long, Set<String>> et : res.entrySet()) {
			for(String ett : et.getValue()) {
				c++;
				System.out.println(c + ".\t" + ett + "\t" + e.getDocName(ett));
				if(c >= 10)
					break;
			}
			if(c >= 10)
				break;
			
		}
		if(c == 0)
			System.out.println("No Results Found");
		double end = System.currentTimeMillis();
		System.out.println("\n\n" + c + " results in " + (end-start)/1000 + "sec");
	}
}
