import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class Spliter {
	static long n = 17640866;
	static long posting_size = 0;
	static long dict_size = 0;
	static String FINAL_FOLDER = "MultiLevel";
	static long k = 0;
	static String[] gMap = new String[1024];
	BufferedReader getFiles(String file) {
		File f = new File(file);
		BufferedReader br = null; 
		try {
			br = new BufferedReader(new FileReader(f));
			//System.out.println("File Found");
		}
		catch(Exception e) {
			System.out.println("File not read");
		}
		return br;
	}
	
	public static void writeToFile(String Folder) {
		try{
			File directory = new File(Folder);
		    if (! directory.exists()){
		        directory.mkdir();
		    }
		    ++k;
		    String filename = Folder + File.separatorChar + "0_" + String.valueOf(k) + ".txt";
		    PrintWriter writer = new PrintWriter(new FileOutputStream( new File(filename),true)); 
			//String line = "";
			for(int i=0; i<1024; i++) {
				writer.println(gMap[i]);
			}
			//Close File
			writer.close();
		}
		catch(IOException e) {
			System.out.println("Error: In writeToFile :Index Creation");
		}
	}
	
	public static void checkSizeAndWrite(String folder) {
		if(dict_size + posting_size > 1024) {
			writeToFile(folder);
			clearMap();
		}
	}
	
	public static void clearMap() {
		posting_size = 0;
		dict_size = 0;
	}
	
	public void ReadAndMap(String fname) {
		BufferedReader br = getFiles(fname);
		String line = "a";
		
		int k=0;
		while(line != "" && line != null) {
			try {
				line = br.readLine();
				gMap[k] = line;
				k++;
				if(k == 1024) {
					writeToFile(FINAL_FOLDER);
					k=0;
				}
			}
			catch(Exception e) {
				//System.out.println(line);
				
			}
		}
		if(k!=0) {
			writeToFile(FINAL_FOLDER);
		}
	}
	public static void main(String []args) {
		System.out.println("Started");
		String filename = args[0];
		Spliter t = new Spliter();
		t.ReadAndMap(filename);
		System.out.println("Finished.");
	}
	
}
