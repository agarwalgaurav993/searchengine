#/bin/bash

date "+%H:%M:%S   %d/%m/%y"
export _JAVA_OPTIONS="-Xms1024m -Xms2048m"
#Run File Extractor (SearchEngine)
#Input : Path to XML File
echo "Extract Started"
java -jar Extractor.jar $1
echo "Extract Ended."

#Run Merger
#Input : Path to Folder where above files are Extracted
#Output : It will form MultiLevel_*
echo "Merge Started"
java -jar Merger.jar
echo "Merge Ended"

#Run Spitter
#Output : Splitted Sorted files
folder=$(ls | grep "MultiLevel_[0-9]" | sort|tail -1)
folder=$folder/1.txt
echo "Split Started"
java -jar Splitter.jar $folder
echo "Split Ended"

#Run MultiLevel Creator on Title Files
echo "MultiLeveling on titles"
java -jar MultiLevelIndexer.jar Title

#Run MultiLevel Creator on Spiltted Files 
echo "MultiLeveling on indexes"
java -jar MultiLevelIndexer.jar MultiLevel

#Clean Up
rm -r MultiLevel_*
rm -r Docs/*

date "+%H:%M:%S   %d/%m/%y"
echo "Done."
