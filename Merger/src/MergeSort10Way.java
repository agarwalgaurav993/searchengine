import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeMap;


public class MergeSort10Way {
	static long n = 17640866;
	static long posting_size = 0;
	static long dict_size = 0;
	static long k=0;
	static String FINAL_FOLDER = "MultiLevel_";
	static TreeMap<String, String > gMap = new TreeMap<String, String >();
	static String DOCUMENT_FOLDER = "Docs";
	
	public class NodeComparator implements Comparator<Node>
	{
	    @Override
	    public int compare(Node x, Node y)
	    {
	    	return x.getKey().compareTo(y.getKey());
	    }
	}
	
	List<BufferedReader> getFiles(String folder) {
		File path = new File(folder);
		List<File> result = new ArrayList<File>();
		//FilenameFilter filter = new Filter(".txt");
		String []children = path.list();
		if(children != null) {
			for(int i=0; i<children.length; i++) {
				String filename = children[i];
				filename = path.getAbsolutePath() + File.separator + filename;
				result.add(new File(filename));
			}
		}
		
		List<BufferedReader> lbr = new ArrayList<BufferedReader>();;
		for(File f : result) {
			try {
				BufferedReader br = new BufferedReader(new FileReader(f));
				lbr.add(br);
			}
			catch(Exception e) {
				System.out.println("File not read");
			}
		}
		return lbr;
	}
	
	public static boolean addPostingList(String Key, String val) {
		posting_size += val.length();
		String res;
		if(!gMap.containsKey(Key)) {
			gMap.put(Key, new String(""));	
			res = val;
		}
		else {
			res = gMap.get(Key) + "|" + val;
		}
		gMap.put(Key, res);
		dict_size = gMap.size();
		return true;
	}
	
	public static void checkSizeAndWrite(String folder) {
		if(dict_size + posting_size > 1024*1024) {
			writeToFile(folder);
			clearMap();
		}
	}
	
	public static void writeToFile(String Folder) {
		try{
			File directory = new File(Folder);
		    if (! directory.exists()){
		        directory.mkdir();
		    }
		    String filename = Folder + File.separatorChar + String.valueOf(k) + ".txt";
		    PrintWriter writer = new PrintWriter(new FileOutputStream( new File(filename),true)); 
			String line = "";
			for(Map.Entry<String, String > e : gMap.entrySet()) {
				line += e.getKey() + "=" + e.getValue();
				writer.println(line);
				line = "";
			}
			//Close File
			writer.close();
		}
		catch(IOException e) {
			System.out.println("Error: In writeToFile :Index Creation");
		}
	}
	
	public static void clearMap() {
		gMap.clear();
		posting_size = 0;
		dict_size = 0;
	}
	
	void mergeAndWriteThem(PriorityQueue<Node> queue, String folder) {
		String prev = "";
		k++;
		while(!queue.isEmpty()) {
			Node node = queue.peek();
			if(node.getKey() != prev) {
				checkSizeAndWrite(folder);
			}
			prev = node.getKey();
			queue.remove();
			/*Add Node to gMap*/
			addPostingList(node.getKey(), node.getValue());
			/*BufferedReader Work*/
			BufferedReader br = node.getBr();
			try {
				String row = br.readLine();
				if(row == null)
					throw new IOException();
				queue.add(new Node(row, br));
				
			} catch (IOException e) {
				/*Do Nothing*/
				
			}
		}
		if(!gMap.isEmpty()) {
			writeToFile(folder);
			clearMap();
		}
		
	}
	
	public void KWayMergeSort(){
		Comparator<Node> comparator = new NodeComparator();
		PriorityQueue<Node> queue = new PriorityQueue<Node>(10, comparator);
		int t = 0;
		List<BufferedReader> lbr = getFiles(DOCUMENT_FOLDER);
		while(lbr.size() > 1) {
			t++;
			for(long i=0; i<lbr.size(); i+=10) {
				for(long j=i; j<i+10 && j<lbr.size(); j++) {
					BufferedReader br = lbr.get((int)j);
					try {
						String row = br.readLine();
						if(row == null)
							throw new IOException();
						queue.add(new Node(row, br));
						
					} catch (IOException e) {
						/*Do Nothing*/
					}
				}
				mergeAndWriteThem(queue, FINAL_FOLDER + String.valueOf(t));
				queue.clear();
			}
			k=0;
			System.out.println("Level " + t + "done.");
			lbr = getFiles(FINAL_FOLDER + String.valueOf(t));
		}
		System.out.println("Level " + t + "done.");
	}
	
	public static void main(String []args) {
		MergeSort10Way m = new MergeSort10Way();
		m.KWayMergeSort();
		System.out.println("Done.");
	}
}
