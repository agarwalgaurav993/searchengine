import java.io.BufferedReader;
import java.util.TreeMap;

public class Node {
	String key;
	String value;
	public String getValue() {
		return value;
	}
	BufferedReader br;
	
	public String getKey() {
		return key;
	}
	public BufferedReader getBr() {
		return br;
	}
	public void setBr(BufferedReader br) {
		this.br = br;
	}
	
	Node(String s, BufferedReader b){
		key = s.split("=")[0];
		value = s.split("=")[1];
		br = b;
	}
	TreeMap<String, TreeMap<String, Long> >  getMyDocMap(){
		String []ar = { "b", "c",  "i", "t", "x"};
		TreeMap<String, TreeMap<String, Long> > t = new TreeMap<String, TreeMap<String, Long> >();
		String[] doc = value.split("\\|");
		for(String d : doc) {
			String docID = d.split(":")[0];
			String fields = d.split(":")[1];
			TreeMap<String, Long> f = new TreeMap<String, Long>();
			String[] vals = fields.split(",");
			for(int i=0; i<vals.length; i++) {
				f.put(ar[i], new Long(vals[i]));
			}
			t.put(docID, f);
		}
		return t;
	}

}
